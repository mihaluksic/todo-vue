import firebase from 'firebase/app';
import 'firebase/firestore';

var config = {
    apiKey: 'AIzaSyBJNvAoqBaWSOGMwhxK0JGqbaoI906eCHg',
    authDomain: 'todo-app-7b113.firebaseapp.com',
    databaseURL: 'https://todo-app-7b113.firebaseio.com',
    projectId: 'todo-app-7b113',
    storageBucket: 'todo-app-7b113.appspot.com',
    messagingSenderId: '141220108131'
};
firebase.initializeApp(config);
const db = firebase.firestore();

db.settings({
    timestampsInSnapshots: true
});

export default db;
